/*try...catch доречно використовувати, коли є потреба перехопити помилки, що можуть виникнути, чи до прикладу генерувати власні помилки*/

const books = [
{ author: "Люсі Фолі",
  name: "Список запрошених",
  price: 70 }, 
{ author: "Сюзанна Кларк",
  name: "Джонатан Стрейндж і м-р Норрелл"}, 
{ name: "Дизайн. Книга для недизайнерів.",
  price: 70}, 
{ author: "Алан Мур",
  name: "Неономікон",
  price: 70}, 
{ author: "Террі Пратчетт",
  name: "Рухомі картинки",
  price: 40},
{ author: "Анґус Гайленд",
  name: "Коти в мистецтві"} ]
            
  for (let i = 0; i < books.length; i++){
  try {const book = books[i];
  
  if (book.author == undefined) {throw new SyntaxError("Неповні дані: відсутнє поле author");}
  if (book.name == undefined) {throw new SyntaxError("Неповні дані: відсутнє поле name");}
  if (book.price == undefined) {throw new SyntaxError("Неповні дані: відсутнє поле price");}       
  else {let ul = document.createElement('ul'); 
        const div = document.getElementById('root');
        div.appendChild(ul);
        const li = document.createElement ('li');
        li.innerHTML = `Автор: ${book.author}, Ім'я: ${book.name}, Ціна: ${book.price}`;
        ul.appendChild(li)}}
  catch (err) {
    if (err instanceof SyntaxError) {
    console.log( "Error: " + err.message );}  
  }}